var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'joinpasswd';

function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

exports.init = function () {
    console.log("Encrypt init");
}

exports.in = function (val) {
    val.Msg_text = encrypt(val.Msg_text)
    return val
}

exports.out = function (val) {
    val.Msg_text = decrypt(val.Msg_text)
    return val
}

exports.type = -4