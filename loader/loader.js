//Module Loader

function logExceptOnTest(value, string) {
    if (process.env.NODE_ENV !== 'test') {
        if (!string)
            console.log(value);
        else
            console.log(value, string);
    }
}

// Gestionnaire de module
module.exports = class loader {
    constructor() {
        this.mods = new Array();
    }

    getFonctions(file) {
        var fs = require('fs')

        var isWin = process.platform;
        if (isWin == "win32")
            var rd = fs.readFileSync('./modules.list', 'utf8').split('\r\n');
        else
            var rd = fs.readFileSync('./modules.list', 'utf8').split('\n');

        logExceptOnTest("__/ Modules loader start")

        for (var i = 0, x = 0; i < rd.length; i++) {
            var line = rd[i];
            if (line[0] != '#' && line.length > 0) {

                var file = "./modules/" + line + ".mod.js"
                try {
                    fs.accessSync("./loader/" + file);
                    var tmp = require(file)
                    this.mods[x] = tmp;
                    x++;
                    logExceptOnTest('\x1b[33m%s\x1b[0m', "--|" + line + " module Loaded")
                } catch (err) {
                    logExceptOnTest('\x1b[31m%s\x1b[0m', "--|" + line + " module Error")
                    logExceptOnTest('\x1b[31m%s\x1b[0m', "---{{" + file + "}} not found")
                    fs.writeFileSync("./api_log", "Can't find \"" + file + "\" module")
                }

            }

        }
    }

    in(val) {
        if (!val.Msg_data)
            val.Msg_data = new Array()
        for (var i = 0; i < this.mods.length; i++) {
            if (this.mods[i].type <= val.Msg_type) {
                var tmp = this.mods[i].in
                val = tmp(val)
            }
        }

        return val
    }

    out(val) {
        this.mods = this.mods.reverse()

        for (var i = 0; i < this.mods.length; i++) {
            if (this.mods[i].type <= val.Msg_type) {
                var tmp = this.mods[i].out
                val = tmp(val)
            }
        }

        return val
    }

    init() {
        logExceptOnTest("______________", "")
        logExceptOnTest("Init modules")
        logExceptOnTest("", "/------------")
        for (var x in this.mods) {
            var tmp = this.mods[x].init;
            tmp()
        }
        logExceptOnTest("", "\\------------")
    }


    run(val) {
        if (this.mods[val]) {
            var tmp = this.mods[val]
            tmp.init();
        }
    }

};
