'use strict';
var pug = require('pug')
var path = require('path');
var app_env = require('dotenv').config();

exports.base = function (req, res) {
  var list = []

  list.push({
    link: "/doc/main",
    type: "GET",
    route: "/",
    description: "main page"
  })
  list.push({
    link: "/doc/oauth/token",
    type: "POST",
    route: "/oauth/token",
    description: "get a administrator token"
  })
  list.push({
    link: "/doc/users",
    type: "POST",
    route: "/users",
    description: "list of users"
  })
  list.push({
    link: "/doc/users/id_",
    type: "GET",
    route: "/users/_id",
    description: "get user information"
  })
  list.push({
    link: "/doc/users/getid",
    type: "POST",
    route: "/getId",
    description: "get user Id"
  })
  list.push({
    link: "/doc/users/update_",
    type: "PUT",
    route: "/users/_id",
    description: "update user information"
  })
  list.push({
    link: "/doc/users/delete_",
    type: "DELETE",
    route: "/users",
    description: "remove user"
  })
  list.push({
    link: "/doc/users/profil_picture",
    type: "POST",
    route: "/upload_profil_picture",
    description: "upload a profil picture"
  })
  list.push({
    link: "/doc/login",
    type: "POST",
    route: "/login",
    description: "login system"
  })
  list.push({
    link: "/doc/position",  
    type: "POST",
    route: "/position",
    description: "update user position"
  })
  list.push({
    link: "/doc/chat/send_",
    type: "POST",
    route: "/chat/sendMessage",
    description: "send a message"
  })
  list.push({
    link: "/doc/chat/get_",
    type: "POST",
    route: "/chat/getMessages",
    description: "get user messages"
  })
  list.push({
    link: "/doc/db/users",
    type: "POST",
    route: "/clear/users",
    description: "clear the users db"
  })
  list.push({
    link: "/doc/db/messages",
    type: "POST",
    route: "/clear chat",
    description: "clear the messages db"
  })

  var html = pug.renderFile(path.resolve('public/views/main.pug'), {
    list: list
  });
  res.send(html)
}

exports.upload = function (req, res) {
  var html = pug.renderFile(path.resolve('public/views/upload.pug'), {
    title: 'Upload Avatar',
    avatar_field: process.env.AVATAR_FIELD
  });
  res.send(html)
}

exports.doc = function (req, res) {
  var html = pug.renderFile(path.resolve('public/doc/' + req.params.route + '.pug'));
  res.send(html)
}

exports.test = function (req, res) {
  res.json({
    message: 'This is an exemple'
  });
}