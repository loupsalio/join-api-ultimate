'use strict';
const mongoose = require('mongoose');
const ConversationModel = mongoose.model('Conversation');
const UserModel = mongoose.model('Users');
const MessageModel = mongoose.model('Message');
const loader = require('loader');
const elem = new loader();

/**
 * Define types for messages
 */
const MESSAGE_TYPES = {
  TEXT: 0, // standard text message
  PICTURE: 1, // picture type (to be rendered as picture with URL in content)
  MISC: 2 // miscellaneous messages (status update, isTyping, etc...) not saved in DB
};

const Event = (name, payload) => JSON.stringify({ event: name, data: payload });
/*
 * Setup websocket server for chat real time services
 */
let rooms = new Map();

exports.setupRealTimeService = async (socket, request) => {
  /**
   * Handle user connection to chat service.
   * User is authentified by his session cookie and must provide
   * a conversation id to connect to in the query parameter.
   */
  console.log('conenction!');
  console.log(request.query);

  const user = request.user;
  let conversation_id;

  try {
    conversation_id = request.query.conversation_id;
  } catch (error) {
    return socket.send(Event('error', 'No conversation id provided.'));
  }
  /**
   * Verify provided conversation exist and user has access rights
   */
  try {
    const conversation = await ConversationModel.findById(
      conversation_id
    ).exec();
    if (conversation == null)
      return socket.send(Event('error', 'Conversation not found.'));
    else if (!conversation.peers.some(peer => peer.equals(user.id)))
      return socket.send(
        Event('error', 'User not in conversation. Access forbidden.')
      );
  } catch (error) {
    if (error instanceof mongoose.CastError)
      return socket.send(Event('error', `Invalid conversation ID.`));
    return socket.send(
      Event('error', `Error fetching conversation: ${error.message}`)
    );
  }

  /**
   * Connect client to conversation
   */
  try {
    // Fetch conversation messages
    const messages = await MessageModel.find({
      conversation: conversation_id
    })
      .sort({ createdAt: 1 })
      .exec();
    // Send previous message to newly connected client to sync
    messages.forEach(msg => socket.send(Event('message', msg)));
    // Join channel to subscribe to message feed
    if (rooms.has(conversation_id)) {
      rooms.get(conversation_id).set(user._id, socket);
    } else {
      const room = new Map();
      room.set(user._id, socket);
      rooms.set(conversation_id, room);
    }
    console.log(rooms);
  } catch (error) {
    return socket.send(Event('error', error.message));
  }

  socket.on('close', async () => {
    console.log(`User ${user._id} diconnected from room ${conversation_id}`);
    rooms.get(conversation_id).delete(user._id);
  });
  /**
   * Register new message event handlers
   */
  socket.on('message', async msgData => {
    console.log('new message!');
    console.log(msgData);
    const msg = JSON.parse(msgData);
    try {
      // Save new message in DB with reference to current conversation
      const saved_msg = await MessageModel({
        conversation: conversation_id,
        sender: user._id,
        content: msg.content,
        type: msg.type
      }).save();
      // Broadcast new message to channel
      const room = rooms.get(conversation_id);
      console.log(room);
      for (var socket of room.values()) {
        socket.send(Event('message', saved_msg));
      }
    } catch (error) {
      return socket.send(
        Event('error', `Error delivering message: ${error.message}`)
      );
    }
  });
};

elem.getFonctions();
elem.init();

/** Conversation managment endpoints */

/**
 * Create conversation with logged in user and a specified list of
 * peer ids.
 */
exports.create_conversation = async (req, res) => {
  try {
    // sanitize peers input and add self
    const peer_ids = [
      ...new Set(req.body.peers.concat([req.user._id.toString()]))
    ];
    peer_ids.sort();
    // find existing conversation
    const conversation = await ConversationModel.findOne({
      peers: peer_ids
    }).exec();

    // if conversation already exist, return it
    if (conversation) return res.json(conversation);
    // check peers ids validity
    try {
      const peers_profile = await Promise.all(
        peer_ids.map(peer_id => UserModel.findById(peer_id).exec())
      );
      if (peers_profile.includes(null))
        return res.status(400).send({
          message: 'Peer not found.'
        });
    } catch (error) {
      console.error(error);
      return res.status(400).send({
        message: 'Invalid peer id.' + JSON.stringify(error)
      });
    }
    // else create a new one
    const saved_conversation = await ConversationModel({
      peers: peer_ids
    }).save();
    return res.status(200).send(saved_conversation);
  } catch (error) {
    console.log(error);
    return res.status(400).send({
      message: error
    });
  }
};

/**
 * Get logged in user's list of conversation
 */
exports.get_conversations = async (req, res) => {
  try {
    const conversations = await ConversationModel.find({
      peers: req.user._id
    }).exec();
    return res.status(200).send(conversations);
  } catch (error) {
    return res.status(400).send({
      message: error.message
    });
  }
};

exports.update_conversation = async (req, res) => {
  try {
    const conversation = await ConversationModel.findById(
      req.body.conversation_id
    ).exec();
    if (conversation == null)
      return res.status(404).send({ message: 'Conversation not found.' });
    if (!conversation.peers.includes(req.user._id))
      return res.status(403).send({ message: 'User not authorized.' });
    // no OP for now as conversation contain no additional information
    return res.status(200).send({ message: 'Conversation updated' });
  } catch (error) {
    return res.status(400).send({
      message: err
    });
  }
};
exports.delete_conversation = async (req, res) => {
  try {
    const conversation = await ConversationModel.findById(
      req.body.conversation_id
    ).exec();
    if (conversation == null)
      return res.status(404).send({ message: 'Conversation not found.' });
    if (!conversation.peers.includes(req.user._id))
      return res.status(403).send({ message: 'User not authorized.' });

    // remove user id from conversation
    conversation.peers = conversation.peers.filter(
      peerId => peerId != req.user._id.toString()
    );
    // if conversation contain only 1 peer (one to one) remove it
    // else, update it
    if (conversation.peers.length <= 1) {
      await conversation.remove();
      return res.status(200).send({ message: 'Conversation deleted.' });
    } else {
      await conversation.save();
      return res
        .status(200)
        .send({ message: 'User removed from conversation.' });
    }
  } catch (error) {
    return res.status(400).send({
      message: error
    });
  }
};

/**
 * Get N message form conversation from conversation id
 */
exports.get_messages = async (req, res) => {
  try {
    // make sure the conversation exists and user belong to it
    const conversation = await ConversationModel.findById(
      req.params.conversation_id
    ).exec();
    if (conversation == null)
      return res.status(404).send({ message: 'Conversation not found.' });
    if (!conversation.peers.includes(req.user._id))
      return res.status(403).send({ message: 'User not authorized.' });
    const messages = await MessageModel.find({
      conversation: req.params.conversation_id
    })
      .sort({ createdAt: 1 })
      .limit(parseInt(req.query.limit))
      .exec();
    return res.status(200).send(messages);
  } catch (error) {
    return res.status(400).send({
      message: error.message
    });
  }
};
