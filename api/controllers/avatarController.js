"use strict";
var mongoose = require("mongoose"),
  User = mongoose.model("Users"),
  multer = require("multer"),
  fs = require("fs"),
  efs = require("fs-extra");

exports.getElements = (req, res) => {
  User.findOne({ mail: "admin@admin.fr" }, (err, elem) => {
    var list = {};
    if (err) res.json(err);
    if (!elem)
      res.json({
        message: "Admin account not found. Contact admin@under-wolf.eu"
      });
    var i = 0;
    elem.avatar.forEach(element => {
      list[i] = { name: element.name, elems: {} };
      for (
        var o = 0;
        fs.existsSync(
          process.env.base_directory +
            "/ressources/avatar/" +
            element.name +
            "/" +
            o +
            ".png"
        );
        o++
      ) {
        list[i]["elems"][o] =
          "http://51.68.46.228:8080/avatar/" + element.name + "/" + o + ".png";
      }
      i++;
    });
    res.json(list);
  });
};

var name_new_element = type => {
  var o;
  for (
    var o = 0;
    fs.existsSync(
      process.env.base_directory +
        "/ressources/avatar/" +
        type +
        "/" +
        o +
        ".png"
    );
    o++
  ) {}
  return o;
};

exports.addSubElement = (req, res) => {
  var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
      // console.log(req.body.type);
      callback(
        null,
        process.env.base_directory + "/ressources/avatar/" + req.body.type + "/"
      );
    },
    filename: function(req, file, callback) {
      // console.log(req.body.type);
      callback(null, name_new_element(req.body.type) + ".png");
    }
  });

  var upload = multer({
    storage: Storage
  }).array("new_elem", 1);

  upload(req, res, function(err) {
    if (err) {
      return res.status(401).json({
        message: err
      });
    }
    if (req.files == undefined || req.files.length == 0)
      return res.status(401).json({
        message: "File can't be empty"
      });
    // console.log("File uploaded : " + req.files[0].filename);
    res.json({ message: "success" });
  });
};

exports.removeSubElement = (req, res) => {
  var tmp = req.body.path.split("/");
  var file = tmp.pop();
  var type = tmp.pop();
  var value = parseInt(file, 10);
  file = process.env.base_directory + "/ressources/avatar/" + type + "/" + file;
  fs.unlinkSync(file);
  for (
    var o = value + 1;
    fs.existsSync(
      process.env.base_directory +
        "/ressources/avatar/" +
        type +
        "/" +
        o +
        ".png"
    );
    o++
  ) {
    fs.renameSync(
      process.env.base_directory +
        "/ressources/avatar/" +
        type +
        "/" +
        o +
        ".png",
      process.env.base_directory +
        "/ressources/avatar/" +
        type +
        "/" +
        (o - 1) +
        ".png"
    );
  }
  res.redirect("https://join.under-wolf.eu/avatar");
  // res.redirect("http://joinweb/avatar");
};

exports.add_element = function(req, res) {
  //  Add element avatar bo

  var new_folder;

  User.find(function(err, list) {
    new_folder = list[0].avatar[0].name;
    list.forEach(function(elem) {
      elem.avatar.push({
        name: req.body.name,
        value: "http://51.68.46.228:8080/avatar/" + req.body.name + "/0.png"
      });
      elem.save();
    });

    var source =
      process.env.base_directory + "/ressources/avatar/" + new_folder;
    var destination =
      process.env.base_directory + "/ressources/avatar/" + req.body.name;
    efs.ensureDirSync(destination);
    // copy source folder to destination
    efs.copy(source, destination, function(err) {
      if (err) {
        console.log("An error occured while copying the folder.");
        res.json({ message: err });
        return console.error(err);
      }
      res.redirect("https://join.under-wolf.eu/avatar");
      console.log("Copy completed!");
    });
  });
};

exports.del_element = function(req, res) {
  //  Remove element avatar bo

  User.find(function(err, list) {
    list.forEach(function(elem) {
      if (elem.avatar.length == 0)
        return res.redirect("https://join.under-wolf.eu/avatar");
      var index = 0;
      var i = -1;
      for (var element of elem.avatar) {
        if (element.name == req.body.name) {
          i = index;
          break;
        }
        index++;
      }
      if (i >= 0) {
        var tmp = elem.avatar.splice(i, 1);
        // elem.avatar = tmp
      }
      elem.save();
    });
    return res.redirect("https://join.under-wolf.eu/avatar");
  });
};
