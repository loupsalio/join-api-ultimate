"use strict";
var mongoose = require("mongoose"),
  User = mongoose.model("Users"),
  nodemailer = require("nodemailer"),
  multer = require("multer"),
  validator = require("email-validator"),
  pug = require("pug"),
  path = require("path"),
  distance = require("gps-distance"),
  crypto = require("crypto");

exports.dev = function(req, res) {
  // Test code here
  res.end(
    "This is a dev page, used to show informations on dev env. Does nothing on prod env."
  );
};

exports.change_avatar = function(req, res) {
  if (!req.params.mail || !req.params.avatar)
    return res.status(403).json({ message: "Missing paramater" });
  User.findOne({ mail: req.params.mail }, (err, elem) => {
    if (err) return res.status(403).json({ message: "user not found" });
    elem.avatar_elem = req.params.avatar;
    elem.save();
    return res.json({ message: "done" });
  });
};

exports.action_disable = function(req, res) {
  if (!req.params.mail || !req.params.mail_s)
    return res.status(403).json({ message: "Missing paramater" });
  User.findOne({ mail: req.params.mail }, (err, elem) => {
    if (err) return res.status(403).json({ message: "user not found" });
    elem.action = 0;
    elem.save();
    User.findOne({ mail: req.params.mail_s }, (err, x) => {
      if (err) return res.status(403).json({ message: "user not found" });
      x.action = 0;
      x.save();
      return res.json({ message: "done" });
    });
  });
};

exports.action_enable = function(req, res) {
  if (!req.params.mail || !req.params.mail_s)
    return res.status(403).json({ message: "Missing paramater" });
  User.findOne({ mail: req.params.mail }, (err, elem) => {
    if (err) return res.status(403).json({ message: "user not found" });
    elem.action = 1;
    elem.save();
    User.findOne({ mail: req.params.mail_s }, (err, x) => {
      if (err) return res.status(403).json({ message: "user not found" });
      x.action = 1;
      x.save();
      return res.json({ message: "done" });
    });
  });
};

exports.connected = function(req, res) {
  if (req.user) {
    var tmp = req.user;
    tmp.verif = 0;
    tmp.hashedPassword = null;
    tmp.salt = null;
    return res.json(tmp);
  } else {
    res.status(403).json({ message: "Not connected" });
  }
};

/* Mail conf */

var smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: "joineiptek@gmail.com",
    pass: "starwolf*"
  }
});

/* Fonctions Javascript */

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

/* Fonctions d'application */

function login(mail, password, res) {
  User.findOne(
    {
      mail: mail
    },
    function(err, User) {
      if (err) {
        console.error(err);
        return res.json({
          message: "Error"
        });
      }
      if (!User) {
        return res.status(401).json({
          message: "Email not found"
        });
      }
      if (
        crypto
          .createHmac("sha1", User.salt)
          .update(password)
          .digest("hex") != User.hashedPassword
      ) {
        return res.status(401).json({
          message: "Wrong password"
        });
      }
      if (!User.active) {
        return res.status(401).json({
          message: "Account not verified"
        });
      }
      User.hashedPassword = "";
      User.salt = "";
      return res.json(User);
    }
  );
}

/* Fonctions liés au model User */

exports.login = (req, res) => {
  console.log(req.body.mail);
  login(req.body.mail, req.body.password, res);
};

exports.upload = (req, res) => {
  var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, process.env.base_directory + process.env.AVATAR_STORAGE);
    },
    filename: function(req, file, callback) {
      callback(
        null,
        file.fieldname + "_" + Date.now() + "_" + file.originalname
      );
    }
  });
  var upload = multer({
    storage: Storage
  }).array(process.env.AVATAR_FIELD);
  upload(req, res, function(err) {
    if (err) {
      return res.status(401).json({
        message: err
      });
    }
    if (req.files == undefined || req.files.length == 0)
      return res.status(401).json({
        message: "File can't be empty"
      });
    console.log("File uploaded : " + req.files[0].filename);
    User.findOneAndUpdate(
      {
        mail: req.body.Name
      },
      {
        profil_picture:
          "http://51.68.46.228:8080/profil_pictures/" + req.files[0].filename
      },
      {
        new: true
      },
      function(err, User) {
        if (err)
          return res.status(401).json({
            message: err
          });
        if (!User)
          return res.status(401).json({
            message: "User not found"
          });
        res.redirect("https://join.under-wolf.eu/profile");
      }
    );
  });
};

exports.list_all_users = (req, res) => {
  User.find({}, function(err, user) {
    user.forEach(element => {
      element.hashedPassword = "";
      element.salt = "";
      element.verif = 0;
    });
    if (err)
      return res.status(401).json({
        message: err
      });
    res.json(user);
  });
};

exports.create_a_user = (req, res) => {
  var new_user = new User(req.body);
  new_user.role = 0;
  new_user.active = false;
  new_user.verif = 0;
  if (!validator.validate(new_user.mail))
    return res.json({
      message: "Wrong email syntax"
    });
  if (new_user.password.length < 6)
    return res.status(449).json({
      message: "Password too short"
    });
  User.find(
    {
      mail: req.body.mail
    },
    function(err, Users) {
      if (Users.length == 0) {
        var rand, mailOptions, link;
        rand = getRandomInt(0, 9999999);
        link = "http://51.68.46.228:8080/verify?id=" + rand;
        var body = pug.renderFile(path.resolve("public/mail.pug"), {
          url: link
        });
        mailOptions = {
          from: '"Join" <loupsalio@gmail.com>',
          to: new_user.mail,
          subject: "Please confirm your Email account",
          html: body
        };
        smtpTransport.sendMail(mailOptions, function(err, response) {
          if (err) {
            return res.status(401).json({
              message: err
            });
          } else {
            console.log("test");
            User.find(
              {
                mail: "admin@admin.fr"
              },
              function(err, elem_admin) {
                if (err) {
                  return res.status(401).json({
                    message: err
                  });
                }
                new_user.verif = rand;
                new_user.avatar = elem_admin[0].avatar;
                console.log(elem_admin[0]);
                new_user.save(function(err, elems) {
                  if (err)
                    return res.status(401).json({
                      message: err
                    });
                  res.json({
                    message: "Verification mail sent to " + elems.mail
                  });
                });
              }
            );
          }
        });
      } else
        return res.status(401).json({
          message: "User Mail already used"
        });
    }
  );
};

exports.read_a_user = (req, res) => {
  User.findOne(
    {
      mail: req.params.mail
    },
    function(err, User) {
      if (err)
        return res.status(401).json({
          message: err
        });
      if (!User)
        return res.end("User with " + req.params.mail + " as mail not found");
      if (!User.active) {
        return res.status(401).json({
          message: "Account not verified"
        });
      } else {
        User.hashedPassword = "";
        User.salt = "";
        User.verif = 0;
        res.json(User);
      }
    }
  );
};

exports.read_a_user_by_id = (req, res) => {
  User.findById(req.params.id, function(err, User) {
    if (err)
      return res.status(401).json({
        message: err
      });
    if (!User)
      return res.end("User with " + req.params.id + " as id not found");
    // if (!User.active) {
    //   return res.status(401).json({
    //     message: "Account not verified"
    //   });
    // } else {
    User.hashedPassword = "";
    User.salt = "";
    User.verif = 0;
    res.json(User);
  });
};

exports.update_a_user = (req, res) => {
  // if (req.params.mail == "admin@admin.fr")
  //   return res.status(401).json({
  //     message: "Admin user can't be edited"
  //   });
  User.findOne(
    {
      mail: req.body.mail
    },
    function(err, new_user) {
      if (err)
        return res.status(401).json({
          message: err
        });
      if (new_user && req.body.mail != req.params.mail) {
        return res.status(401).json({
          message: req.body.mail + " already used"
        });
      }
      var rand;
      if (req.body.mail != req.params.mail) {
        rand = getRandomInt(0, 9999999);
        req.body.verif = rand;
      }
      User.findOneAndUpdate(
        {
          mail: req.params.mail
        },
        req.body,
        {
          new: true
        },
        function(err, User) {
          if (err)
            return res.status(401).json({
              message: err
            });
          if (!User) {
            return res.status(401).json({
              message: "User not found"
            });
          }
          if (req.body.mail == req.params.mail)
            return res.json({
              message: "User account edited successfully"
            });
          else {
            var mailOptions, link;
            link = "http://51.68.46.228:8080/verify?id=" + rand;
            var body = pug.renderFile(path.resolve("public/mail.pug"), {
              url: link
            });
            mailOptions = {
              from: '"Join" <loupsalio@gmail.com>',
              to: req.body.mail,
              subject: "Account edited, please confirm your new Email",
              html: body
            };
            smtpTransport.sendMail(mailOptions, function(err, response) {
              if (err) {
                return res.status(401).json({
                  message: err
                });
              } else {
                return res.json({
                  message:
                    "User account edited successfully, email sent to " +
                    req.body.mail
                });
              }
            });
          }
        }
      );
    }
  );
};

exports.delete_a_user = (req, res) => {
  if (req.params.mail == "admin@admin.fr")
    return res.status(401).json({
      message: "Admin user can't be deleted"
    });
  User.remove(
    {
      mail: req.params.mail
    },
    function(err, User) {
      if (err)
        return res.status(401).json({
          message: err
        });
      res.json({
        message: "User " + req.params.mail + " successfully deleted"
      });
    }
  );
};

exports.getId = (req, res) => {
  User.findOne(
    {
      mail: req.body.mail
    },
    function(err, user) {
      if (err)
        return res.json({
          message: err
        });
      else if (!user) {
        return res.status(401).json({
          message: "User not found"
        });
      } else if (!user.active) {
        return res.status(401).json({
          message: "Account not verified"
        });
      } else {
        res.json({
          id: user._id
        });
      }
    }
  );
};

exports.validate_account = (req, res) => {
  User.findOne(
    {
      verif: req.query.id
    },
    function(err, tmp_user) {
      if (err)
        return res.status(401).json({
          message: err
        });
      if (!tmp_user) {
        var html = pug.renderFile(path.resolve("public/views/verify.pug"), {
          content: "User not found"
        });
        return res.end(html);
      }
      if (tmp_user.active) {
        var html = pug.renderFile(path.resolve("public/views/verify.pug"), {
          content: "Account already activated"
        });
        return res.end(html);
      } else
        User.findOneAndUpdate(
          {
            verif: req.query.id
          },
          {
            active: true
          },
          {
            new: true
          },
          function(err, User) {
            if (err)
              return res.status(401).json({
                message: err
              });
            var html = pug.renderFile(path.resolve("public/views/verify.pug"), {
              content: "Account of " + User.name + " activated"
            });
            return res.end(html);
          }
        );
    }
  );
};

exports.setpos = (req, res) => {
  if (!req.user && !req.body.mail)
    return res.status(401).json({
      error: "Not logged"
    });
  else if (!req.user) req.user = { mail: req.body.mail };

  User.findOne(
    {
      mail: req.user.mail
    },
    function(err, User) {
      if (err)
        return res.status(401).json({
          message: err
        });
      if (!User) {
        return res.status(401).json({
          message: "User not found"
        });
      }
      if (!User.active) {
        return res.status(401).json({
          message: "Account not verified"
        });
      }
      if (User) {
        User.position = JSON.stringify(req.body.position);
        User.last_connection = Date.now();
        User.save(function(err, User) {
          if (err) {
            console.log(err);
            return res.json({
              message: err
            });
          }
          User.hashedPassword = "";
          User.salt = "";
          User.verif = 0;
          console.log(User);
          res.json(User);
        });
      } else
        return res.status(401).json({
          message: "User not found"
        });
    }
  );
};

exports.edit_avatar = (req, res) => {
  User.findOne({ mail: req.body.Name }, function(err, user) {
    if (err)
      return res.status(401).json({
        error: err
      });
    if (!user)
      return res.status(401).json({
        message: "User not found"
      });
    if (!req.body.avatar)
      return res.status(401).json({
        message: "Need an 'avatar' field"
      });

    var base = "http://51.68.46.228:8080/avatar/";
    var i = 0;
    req.body.avatar.split("").forEach(element => {
      if (i < user.avatar.length)
        user.avatar[i].value =
          base + user.avatar[i].name + "/" + element + ".png";
      element;
      i++;
    });
    user.save();
    return res.status(200).json(user);
  });
};

exports.get_avatar = (req, res) => {
  User.findOne({ mail: req.body.mail }, function(err, user) {
    if (err)
      return res.status(401).json({
        error: err
      });
    if (!user)
      return res.status(401).json({
        message: "User not found"
      });
    var base = "http://51.68.46.228:8080/avatar/";
    var list = {};
    user.avatar.forEach(element => {
      list[element.name] = base + element.name + "/" + element.value + ".png";
    });
    return res.status(200).json({ avatar: list });
  });
};

exports.removeUsers = (req, res) => {
  User.remove({}, function(err) {
    console.log("User collection removed");
    var new_user = new User({
      name: "admin",
      lastname: "admin",
      surname: "admin",
      mail: "admin@admin.fr",
      password: "starwolf",
      role: 1,
      active: true
    });
    new_user.save(function(err, User) {
      if (err) console.log(err);
      else {
        console.log("New user - %s:%s", User.mail, new_user.password);
        return res.json({
          message: "User collection removed"
        });
      }
    });
  });
};

exports.add_hobbie = function(req, res) {
  User.findOne(
    {
      mail: req.body.mail
    },
    function(err, elem) {
      if (err) {
        return res.json({ message: err });
      }
      elem.hobbies.push({ name: req.body.name, status: true });
      elem.save();
      return res.redirect("https://join.under-wolf.eu/profile/");
    }
  );
};

exports.del_hobbie = function(req, res) {
  console.log(req.body.mail);
  console.log(req.body.name);
  User.findOne(
    {
      mail: req.body.mail
    },
    function(err, elem) {
      var index = 0;
      var i = -1;
      for (var element of elem.hobbies) {
        if (element.name == req.body.name) {
          i = index;
          break;
        }
        index++;
      }
      if (i >= 0) {
        var tmp = elem.hobbies.splice(i, 1);
      }
      elem.save();
      return res.redirect("https://join.under-wolf.eu/profile/");
    }
  );
};

//send mail for password
exports.check_user_mail = function(req, res) {
  User.findOne({ mail: req.body.email }, function(err, elem) {
    if (err) {
      return res.json({ message: err });
    }
    if (!elem) {
      return res.json({ message: "user not found" });
    }
    elem.active = false;
    elem.save();

    var mailOptions, link;
    link = "https://join.under-wolf.eu/new_password?id=" + elem.verif;
    var body = pug.renderFile(path.resolve("public/mail_password.pug"), {
      url: link
    });
    mailOptions = {
      from: '"Join" <loupsalio@gmail.com>',
      to: elem.mail,
      subject: "Forgot your password",
      html: body
    };
    smtpTransport.sendMail(mailOptions, function(err, response) {
      if (err) {
        return res.status(401).json({
          message: err
        });
      } else {
        return res.redirect("https://join.under-wolf.eu/login");
      }
    });
  });
};

exports.new_password = function(req, res) {
  User.findOne({ verif: req.body.id }, function(err, elem) {
    console.log(elem.hashedPassword);
    elem.hashedPassword = elem.encryptPassword(req.body.new_password);
    console.log(elem.hashedPassword);
    elem.active = true;
    elem.save();
    return res.redirect("https://join.under-wolf.eu/login");
  });
};

exports.close_users = function(req, res) {
  var today = new Date();
  today.setSeconds(today.getSeconds() - 50);
  if (!req.user && !req.body.mail)
    return res.status(401).json({
      error: "Not logged"
    });
  else if (!req.user) req.user = { mail: req.body.mail };
  User.find(function(err, users) {
    User.findOne({ mail: req.body.mail }, function(err, current_user) {
      var current_user_coords = JSON.parse(current_user.position);
      users = users.filter(function(item) {
        try {
          var tmp_user_coords = JSON.parse(item.position);
          var users_distance =
            distance(
              current_user_coords.latitude,
              current_user_coords.longitude,
              tmp_user_coords.latitude,
              tmp_user_coords.longitude
            ) * 1000;
        } catch {
          var users_distance = 1000;
        }
        // console.log(item.last_connection > today)
        // if (item.last_connection > today)
        //   return false
        // if (users_distance > 100 || req.body.mail == item.mail) {
        if (users_distance > 200) {
          console.log(req.body.mail, " ", item.mail, " => ", users_distance);
          return false;
        } else {
          console.log(req.body.mail, " ", item.mail, " => ", users_distance);
          item.verif = users_distance;
          return true;
        }
      });
      users.forEach(element => {
        element.hashedPassword = "";
        element.salt = "";
      });
      return res.json(users);
    });
  });
};

exports.invit_beta = function(req, res) {
  var body = pug.renderFile(path.resolve("public/mail_bienvenue.pug"), {
    url:
      "https://link.under-wolf.eu/Explorateur//join_beta-318b9eafe6f347aa845e49c404de92df-signed.apk"
  });
  if (!req.body.dest)
    return res.json({
      message: "empty mail"
    });
  smtpTransport.sendMail(
    {
      from: '"Join Team" <joineiptek@gmail.com>',
      to: req.body.dest,
      subject: "Invitation Beta",
      html: body
    },
    function(err, response) {
      if (err) {
        return res.status(401).json({
          message: err
        });
      } else {
        return res.status(200).json({
          message: response
        });
      }
    }
  );
};
