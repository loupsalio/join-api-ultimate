'use strict';

const Event = (name, payload) => JSON.stringify({ event: name, data: payload });
/*
 * Setup websocket server for chat real time services
 */
let rooms = new Map();

exports.setupRealTimeService = async (socket, request) => {
  /**
   * Handle user connection to game service.
   * User is authentified by his session cookie and must provide
   * a game room id to connect to in the query parameter.
   */
  console.log('conenction!');
  console.log(request.query);

  const user = request.user;
  let room_id;

  try {
    room_id = request.query.room_id;
  } catch (error) {
    return socket.send(Event('error', 'No room id provided.'));
  }

  socket.on('close', async () => {
    console.log(`User ${user._id} diconnected from room ${room_id}`);
    rooms.get(room_id).delete(user._id);
  });

  /**
   * Register new message event handlers
   */
  socket.on('message', async msgData => {
    console.log('Received new message: ', msgData);
    // const msg = JSON.parse(msgData);
    try {
      // Broadcast new message to room
      const room = rooms.get(room_id);
      console.log(room);
      for (var socket of room.values()) {
        socket.send(Event('message', msgData));
      }
    } catch (error) {
      return socket.send(
        Event('error', `Error delivering message: ${error.message}`)
      );
    }
  });
};
