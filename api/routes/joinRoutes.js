"use strict";
module.exports = function(app) {
  var user = require("../controllers/userController");
  var chat = require("../controllers/chatController");
  var game = require("../controllers/gameController");
  var web = require("../controllers/webController");
  var avatar = require("../controllers/avatarController");
  var passport = require("passport");
  var oauth2 = require("../../Auth/oauth.js");

  app.route("/dev").get(user.dev);

  app.post("/oauth/token", oauth2.token);

  app.route("/").get((req, res) => {
    res.redirect("/docs");
  });

  app.get("/avatar_elem/:mail/:avatar", user.change_avatar);
  app.post("/action_en/:mail/:mail_s", user.action_enable);
  app.post("/action_dis/:mail/:mail_s", user.action_disable);

  // app.route("/login").post(user.login);
  app.route("/login").post(passport.authenticate("local"), function(req, res) {
    var tmp = req.user;
    tmp.verif = 0;
    tmp.hashedPassword = null;
    tmp.salt = null;
    res.json(tmp);
  });

  app.route("/logout").post(function(req, res) {
    req.logout();
    res.json({ message: "Disconnected" });
  });

  app.route("/connected").get(user.connected);

  app.get(
    "/auth/facebook",
    passport.authorize("facebook", {
      // scope: ["mail"]
    })
  );

  app.get(
    "/admin/cb_facebook",
    passport.authenticate("facebook", {
      failureRedirect: "/login"
    }),
    function(req, res) {
      res.redirect(
        process.env.FACEBOOK_JOIN_CALLBACK +
          "/?user=" +
          JSON.stringify(req.user)
      );
      req.logout();
    }
  );

  app.route("/test").post(web.test);

  // app.route('/avatar')
  //   .get(web.upload);

  app.route("/upload_profil_picture").post(user.upload);

  app.route("/doc/:route").get(web.doc);

  app.route("/verify").get(user.validate_account);

  app
    .route("/avatar/edit")
    .get(user.edit_avatar)
    .post(user.edit_avatar);
  app
    .route("/avatar/get")
    .get(user.get_avatar)
    .post(user.get_avatar);

  app
    .route("/users")
    .get(user.list_all_users)
    .post(user.create_a_user);

  app
    .route("/users/:mail")
    .get(user.read_a_user)
    .put(user.update_a_user)
    .delete(user.delete_a_user);

  app.route("/users/id/:id").get(user.read_a_user_by_id);

  app
    .route("/avatar/elements")
    .get(avatar.getElements)
    .post(avatar.getElements);

  app.route("/add_sub_elem_avatar").post(avatar.addSubElement);

  app.route("/delete_sub_elem_avatar").post(avatar.removeSubElement);

  app.route("/add_elem_avatar").post(avatar.add_element);

  app.route("/delete_elem_avatar").post(avatar.del_element);

  app.route("/add_hobbie").post(user.add_hobbie);

  app.route("/delete_hobbie").post(user.del_hobbie);

  app.route("/position").post(user.setpos);

  app.route("/getId").post(user.getId);

  app
    .route("/conversation")
    .post(chat.create_conversation)
    .get(chat.get_conversations)
    .put(chat.update_conversation)
    .delete(chat.delete_conversation);

  app.ws("/conversation", chat.setupRealTimeService);

  app.route("/:conversation_id/message").get(chat.get_messages);

  app.ws("/game", game.setupRealTimeService);

  app.route("/clear/users").get(user.removeUsers);

  app.route("/getId").post(user.getId);

  // Get back Password lost
  app.route("/reset_password").post(user.check_user_mail);
  app.route("/new_password").post(user.new_password);

  app.route("/close_users").post(user.close_users);
  app.route("/invit_beta").post(user.invit_beta);
};

// passport.authenticate('bearer', {
//   session: false
// }),
