"use strict";
const mongoose = require("mongoose");

const MessageSchema = mongoose.Schema(
	{
		conversation: { type: mongoose.Schema.Types.ObjectId, ref: "Conversation" },
		sender: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
		content: { type: String, required: true },
		type: { type: Number, required: true }
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Message", MessageSchema);
