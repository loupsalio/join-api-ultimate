'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var AvatarSchema = new Schema({
  value: {
    type: Number,
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  last_connection: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Avatars', AvatarSchema);