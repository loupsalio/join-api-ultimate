"use strict";
const mongoose = require("mongoose");

const ConversationSchema = mongoose.Schema(
	{
		// arbitrary number of peers in chat (to make model reusable for in-game chats)
		peers: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }]
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Conversation", ConversationSchema);
