"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var crypto = require("crypto");

var UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  facebook: {
    type: String,
    default: ""
  },
  mail: {
    type: String,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  position: {
    type: String,
    default: null
  },
  avatar_elem: {
    type: String,
    default: "lucas"
  },
  active: {
    type: Boolean,
    default: false
  },
  verif: {
    type: Number,
    default: 0
  },
  role: {
    type: Number,
    default: 0
  },
  action: { type: Number, default: 0 },
  avatar: {
    type: [
      {
        name: String,
        value: String
      }
    ],
    default: [
      {
        name: "cheveux",
        value: "http://51.68.46.228:8080/avatar/cheveux/0.png"
      },
      {
        name: "yeux",
        value: "http://51.68.46.228:8080/avatar/yeux/0.png"
      },
      {
        name: "bouche",
        value: "http://51.68.46.228:8080/avatar/bouche/0.png"
      }
    ],
    required: true
  },
  hobbies: {
    type: [
      {
        name: String,
        status: Boolean
      }
    ],
    default: [
      {
        name: "movie",
        status: true
      },
      {
        name: "music",
        status: true
      },
      {
        name: "sport",
        status: true
      },
      {
        name: "book",
        status: true
      }
    ]
  },
  profil_picture: {
    type: String,
    default: "http://51.68.46.228:8080/profil_pictures/base.png"
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  last_connection: {
    type: Date,
    default: Date.now
  }
});

UserSchema.methods.encryptPassword = function(password) {
  return crypto
    .createHmac("sha1", this.salt)
    .update(password)
    .digest("hex");
  //more secure – return crypto.pbkdf2Sync(password, this.salt, 10000, 512);
};

UserSchema.virtual("userId").get(function() {
  return this.id;
});

UserSchema.virtual("password")
  .set(function(password) {
    this._plainPassword = password;
    this.salt = crypto.randomBytes(32).toString("hex");
    //more secure - this.salt = crypto.randomBytes(128).toString('hex');
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() {
    return this._plainPassword;
  });

UserSchema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};

var findOrCreate = require("mongoose-findorcreate");
UserSchema.plugin(findOrCreate);
module.exports = mongoose.model("Users", UserSchema);
