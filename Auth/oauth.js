var oauth2orize = require("oauth2orize");
var passport = require("passport");
var crypto = require("crypto");
var mongoose = require("mongoose");
var UserModel = mongoose.model("Users");
var AccessTokenModel = mongoose.model("AccessTokens");
const FacebookStrategy = require("passport-facebook").Strategy;
var RefreshTokenModel = mongoose.model("RefreshTokens"),
  User = mongoose.model("Users");
const LocalStrategy = require('passport-local').Strategy;

// create OAuth 2.0 server
var server = oauth2orize.createServer();
passport.authorize("facebook", {
  scope: ["email"]
});

passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
  User.findById(id, function (err, User) {
    cb(err, User);
  });
});

// Local strategy

passport.use(new LocalStrategy({
  usernameField: "mail"
},
  function (username, password, done) {
    User.findOne({ mail: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.checkPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }))

// Facebook Oauth strategy

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.API_KEY,
      clientSecret: process.env.API_SECRET,
      callbackURL: process.env.CALLBACK,
      profileFields: [
        "id",
        "displayName",
        "emails",
        "birthday",
        "friends",
        "first_name",
        "last_name",
        "middle_name",
        "gender",
        "link"
      ]
    },
    function (accessToken, refreshToken, profile, cb) {
      // console.log(profile);

      User.findOrCreate(
        {
          facebook: profile.id
        },
        {
          password: "facebook",
          mail: profile.id,
          name: profile.name.middleName,
          surname: profile.name.givenName,
          lastname: profile.name.familyName,
          active: true
        },
        function (err, user) {
          User.findOne({ mail: "admin@admin.fr" }, function (err, elem) {
            console.log(elem)
            user.avatar = elem.avatar
            if (!(user.name, user.surname, user.lastname)) {
              user.name = profile.displayName;
              user.surname = profile.displayName;
              user.lastname = profile.displayName;
              user.active = true;
            }
            user.save();
            return cb(err, user);
          })
        }
      );
    }
  )
);

// Exchange username & password for an access token.
server.exchange(
  oauth2orize.exchange.password(function (client, name, password, scope, done) {
    UserModel.findOne(
      {
        name: name
      },
      function (err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false);
        }
        if (!user.checkPassword(password)) {
          return done(null, false);
        }
        User.findOne(
          {
            name: name
          },
          function (err, User) {
            if (err) return done(null, false);
            if (!User) return done(null, false);
            RefreshTokenModel.remove(
              {
                userId: user.name,
                clientId: client.clientId
              },
              function (err) {
                if (err) return done(err);
              }
            );
            AccessTokenModel.remove(
              {
                userId: user.name,
                clientId: client.clientId
              },
              function (err) {
                if (err) return done(err);
              }
            );

            var tokenValue = crypto.randomBytes(32).toString("hex");
            var refreshTokenValue = crypto.randomBytes(32).toString("hex");
            var token = new AccessTokenModel({
              token: tokenValue,
              clientId: client.clientId,
              userId: user.name
            });
            var refreshToken = new RefreshTokenModel({
              token: refreshTokenValue,
              clientId: client.clientId,
              userId: user.name
            });
            refreshToken.save(function (err) {
              if (err) {
                return done(err);
              }
            });
            var info = {
              scope: "*"
            };
            token.save(function (err, token) {
              if (err) {
                return done(err);
              }
              done(null, tokenValue, refreshTokenValue, {
                expires_in: 3600
              });
            });
          }
        );
      }
    );
  })
);

// Exchange refreshToken for an access token.
server.exchange(
  oauth2orize.exchange.refreshToken(function (
    client,
    refreshToken,
    scope,
    done
  ) {
    RefreshTokenModel.findOne(
      {
        token: refreshToken
      },
      function (err, token) {
        if (err) {
          return done(err);
        }
        if (!token) {
          return done(null, false);
        }
        if (!token) {
          return done(null, false);
        }

        UserModel.findById(token.userId, function (err, user) {
          if (err) {
            return done(err);
          }
          if (!user) {
            return done(null, false);
          }

          RefreshTokenModel.remove(
            {
              userId: user.name,
              clientId: client.clientId
            },
            function (err) {
              if (err) return done(err);
            }
          );
          AccessTokenModel.remove(
            {
              userId: user.name,
              clientId: client.clientId
            },
            function (err) {
              if (err) return done(err);
            }
          );

          var tokenValue = crypto.randomBytes(32).toString("hex");
          var refreshTokenValue = crypto.randomBytes(32).toString("hex");
          var token = new AccessTokenModel({
            token: tokenValue,
            clientId: client.clientId,
            userId: user.name
          });
          var refreshToken = new RefreshTokenModel({
            token: refreshTokenValue,
            clientId: client.clientId,
            userId: user.name
          });
          refreshToken.save(function (err) {
            if (err) {
              return done(err);
            }
          });
          var info = {
            scope: "*"
          };
          token.save(function (err, token) {
            if (err) {
              return done(err);
            }
            done(null, tokenValue, refreshTokenValue, {
              expires_in: 3600
            });
          });
        });
      }
    );
  })
);

// token endpoint
exports.token = [
  passport.authenticate(["basic", "oauth2-client-password"], {
    session: false
  }),
  server.token(),
  server.errorHandler()
];
