var passport = require("passport");
var BasicStrategy = require("passport-http").BasicStrategy;
var ClientPasswordStrategy = require("passport-oauth2-client-password")
	.Strategy;
var BearerStrategy = require("passport-http-bearer").Strategy;
var mongoose = require("mongoose");
var UserModel = mongoose.model("Users");
var ClientModel = mongoose.model("Clients");
var AccessTokenModel = mongoose.model("AccessTokens");
var RefreshTokenModel = mongoose.model("RefreshTokens");

passport.use(
	new BasicStrategy(function(username, password, done) {
		ClientModel.findOne({ clientId: username }, function(err, client) {
			if (err) {
				return done(err);
			}
			if (!client) {
				return done(null, false);
			}
			if (client.clientSecret != password) {
				return done(null, false);
			}

			return done(null, client);
		});
	})
);

passport.use(
	new ClientPasswordStrategy(function(clientId, clientSecret, done) {
		ClientModel.findOne({ clientId: clientId }, function(err, client) {
			if (err) {
				return done(err);
			}
			if (!client) {
				return done(null, false);
			}
			if (client.clientSecret != clientSecret) {
				return done(null, false);
			}

			return done(null, client);
		});
	})
);

passport.use(
	new BearerStrategy(function(accessToken, done) {
		console.log(accessToken);
		AccessTokenModel.findOne({ token: accessToken }, function(err, token) {
			if (err) {
				return done(err);
			}
			if (!token) {
				return done(null, false);
			}

			if (Math.round((Date.now() - token.created) / 1000) > 3600) {
				AccessTokenModel.remove({ token: accessToken }, function(err) {
					if (err) return done(err);
				});
				return done(null, false, { message: "Token expired" });
			}

			UserModel.findOne({ Name: token.userId }, function(err, user) {
				if (err) {
					return done(err);
				}
				if (!user) {
					return done(null, false, { message: "Unknown user" });
				}

				var info = { scope: "*" };
				done(null, user, info);
			});
		});
	})
);
