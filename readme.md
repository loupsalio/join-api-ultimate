# Join API ultimate

[Join](http://join.under-wolf.eu) is a social network of conviviality, based on geolocation.
Each user has a custom avatar, evolving in a graphical environment just like where the user is.
Exchanges are limited to 5 messages, then users can choose to share their locations to meet IRL.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites


```
Docker / Docker-compose
```

### Installing

First check you installed the requisites, then clone the repository and install the dependencies :

```bash
docker-compose up
``` 

## Running the tests

All tests are in the tests/requests.js file

If you want to add some tests, just check the basic fonctions as exemple

```bash
npm test
```

## Routes

Base_URL Port 3030
* [GET] /users
* [POST] /users
* [POST] /users/:Name
* [PUT] /users/:Name
* [DELETE] /users/:Name
* [POST] /getId
* [POST] /position
* [POST] /chat/sendMessage
* [POST] /chat/getMessages
* [POST] /clear/messages
* [POST] /clear/users
* [GET] /clear/avatar
* [POST] /upload
* [POST] /oauth/token


## Modules system

If you want to create a module use this command:

```bash
cp loader/modules/exemple.mod.js loader/modules/<your module name>.mod.js 
```
You will need to add your name module in the modules.list file in the root folder.

Then you can edit your module file :
``` javascript
// Exemple join module

exports.init = function(){
    console.log("Exemple module");
}

exports.in = function(val){
    // Do something on val (input)
    return val
}

exports.out = function(val){
    // Do something on val (output)
    return val
}

exports.type = 0 // Minimum type value (> 0)
```

## Authors

* **Lucas CLEMENCEAU** - *Developer* - [Loupsalio](https://gitlab.com/loupsalio)
