var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
if (!process.env.MONGO_URI)
  process.env.MONGO_URI = "mongodb://localhost/joindb";
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true
});

require("./api/models/userModel");

var User = mongoose.model("Users");
mongoose.set("useCreateIndex", true);

User.remove({}, function(err) {
  if (err) {
    console.log(err);
    return;
  }
  var new_user = new User({
    name: "admin",
    mail: "admin@admin.fr",
    password: "starwolf",
    surname: "admin",
    lastname: "admin",
    active: true
  });
  new_user.save(function(err, User) {
    if (err) console.log(err);
    else console.log("New user - %s:%s", User.mail, new_user.password);
  });
});

setTimeout(function() {
  mongoose.disconnect();
}, 3000);
