/**
 * Constants
 */
const SESSION_SECRET = 'bushdid911';

var express = require('express'),
  app = express(),
  expressWs = require('express-ws')(app),
  port = process.env.PORT || 3030,
  mongoose = require('mongoose'),
  passport = require('passport'),
  bodyParser = require('body-parser'),
  morgan = require('morgan'),
  all_routes = require('express-list-endpoints'),
  fs = require('fs'),
  flash = require('connect-flash'),
  session = require('express-session'),
  MongoDBStore = require('connect-mongodb-session')(session),
  cookieparser = require('cookie-parser'),
  swaggerUi = require('swagger-ui-express');

var expressGoogleAnalytics = require('express-google-analytics');

// Insert your Google Analytics Id, Shoule be something like 'UA-12345678-9'
var analytics = expressGoogleAnalytics('UA-143015147-1');

try {
  var sessionStore = new MongoDBStore({
    uri: 'mongodb://localhost/connect_mongodb_session_test',
    collection: 'mySessions'
  });
} catch (error) {
  console.error('Error setting up mongoDB session store');
  throw error;
}

/* Initialisation */

console.log('\n################################');
console.log('#      Join API started      ##');
console.log('################################\n');

// Models loaders

require('./api/models/userModel');
require('./api/models/conversationModel');
require('./api/models/messageModel');
require('./api/models/clientModel');
require('./api/models/accessTokenModel');
require('./api/models/refreshTokenModel');

// Env and Auth configuration

// EXPRESS MIDDLEWARES
require('dotenv').config();
process.env.base_directory = __dirname;
app.use(express.static(__dirname + '/ressources'));

app.use(cookieparser(SESSION_SECRET));
app.use(
  session({
    secret: SESSION_SECRET,
    name: 'session_cookie',
    store: sessionStore,
    proxy: true,
    resave: false,
    saveUninitialized: true
  })
);
app.use(flash());
app.use(analytics);
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

if (process.env.ENVIRONEMENT == 'dev') app.use(morgan('dev'));

require('./Auth/pass');
app.use(passport.initialize());
app.use(passport.session());

try {
  fs.mkdirSync(__dirname + process.env.AVATAR_STORAGE);
} catch (err) {}

// MongoDB configuration

mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;
//if (!process.env.MONGO_URI)
process.env.MONGO_URI = 'mongodb://localhost/joindb';
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true
});

// Cross origin header control

app.use(function(request, response, next) {
  response.header('Access-Control-Allow-Origin', '*');
  response.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

// Documentation

const swaggerDocument = require('./swagger.json');

const options = {
  swaggerOptions: {
    validatorUrl: null
  },
  customCss: '.swagger-ui .topbar { display: none }',
  customSiteTitle: 'Join API',
  customfavIcon: '/join-ultimate-v3.png'
};

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

// Route importation

app.ws('/', function(ws, req) {
  ws.on('message', function(msg) {
    console.log(msg);
  });
  console.log('socket', req.testing);
});

var routes = require('./api/routes/joinRoutes');
routes(app);

console.log(app);
// Run application listening
app.listen(port);

// Route list debug

console.log('______________');
console.log('Routes list : \x1b[36m');

all_routes(app).forEach(element => {
  element.methods.forEach(type => {
    console.log(' - ' + element.path + ' | ' + type);
  });
});

console.log('\x1b[0m________________________________\n');
console.log('\x1b[32;4mJoin API server started on: ' + port + '\x1b[0m');
