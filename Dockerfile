FROM node:latest

# Définir le répertoire de travail de l'applicaiton à /app
RUN mkdir -p /app/join
WORKDIR /app/join

# Copier le contenu du répertoire courant dans le conteneur à l'adresse /app
ADD . /app/join

# Définir les variable d'environnement
ENV NAME Join_World
ENV MONGO_URI mongodb://mongo/joindb

COPY package.json /app/join/
RUN npm install --quiet
# RUN mkdir ressources/profil_pictures
COPY . /app
RUN cp ressources/base.png /app/join/ressources/profil_pictures

# Rendre le port 3030 accessible à l'extérieur du conteneur
EXPOSE 3030

CMD [ "npm", "start" ]
