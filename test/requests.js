var expect = require("chai").expect,
  fs = require("fs"),
  request = require("request");

process.env.NODE_ENV = "test";

describe("Join API", function () {
  // describe("Join static", function () {

  //   it("module loader instance", function (done) {
  //     var loader = require("loader"),
  //       tmp = new loader;
  //     expect(tmp).not.null;
  //     done()
  //   });

  //   it("encrypt module loaded", function (done) {
  //     var loader = require("loader"),
  //       tmp = new loader;
  //     tmp.getFonctions()
  //     expect(tmp.mods[0]).exist
  //     expect(tmp.mods[1]).exist
  //     done()
  //   });

  //   it("basic message encryption module", function (done) {
  //     var msg = {
  //       Name: "sender",
  //       Target: "receiver",
  //       Msg_type: 0,
  //       Msg_text: "test"
  //     };
  //     var loader = require("loader"),
  //       tmp = new loader;
  //     tmp.getFonctions()
  //     tmp.mods[1].in(msg)
  //     expect(msg.Msg_text).not.equal("test")
  //     tmp.mods[1].out(msg)
  //     expect(msg.Msg_text).equal("test")
  //     done()
  //   });

  // });

  describe("Join non-static", function () {
    var base_url = "http://localhost:3030";

    var token = "";

    // it("returns status 200", function (done) {
    //   request.post(base_url + "/test", function (error, response, body) {
    //     if (!error) {
    //       expect(response.statusCode).to.equal(200);
    //       done();
    //     } else {
    //       console.log("Error : " + error)
    //     }
    //   });
    // });

    // it("POST->/oauth/token", function (done) {
    //   request.post(
    //     base_url + "/oauth/token",
    //     {
    //       json: {
    //         username: "admin",
    //         password: "starwolf",
    //         client_id: "api_root",
    //         client_secret: "starwolf",
    //         grant_type: "password"
    //       }
    //     },
    //     function (error, response, body) {
    //       expect(response.statusCode).to.equals(200);
    //       if (!error && response.statusCode == 200) {
    //         token = response.body.access_token;
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("GET->/", function (done) {
    //   request.get(base_url + "/", function (error, response, body) {
    //     if (!error) {
    //       expect(response.statusCode).to.equal(200);
    //       done();
    //     } else {
    //       console.log("Error : " + error);
    //     }
    //   });
    // });

    // it("POST->/login", function (done) {
    //   request.post(
    //     base_url + "/login",
    //     {
    //       auth: {
    //         bearer: token
    //       },
    //       json: {
    //         mail: "admin@admin.fr",
    //         password: "starwolf"
    //       }
    //     },
    //     function (error, response, body) {
    //       if (!error) {
    //         expect(body.active).is.true;
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("POST->/test", function (done) {
    //   request.post(base_url + "/test", function (error, response, body) {
    //     if (!error) {
    //       expect(body).to.equal('{"message":"This is an exemple"}');
    //       done();
    //     } else {
    //       console.log("Error : " + error);
    //     }
    //   });
    // });

    // it("GET->/avatar", function (done) {
    //   request.get(base_url + "/avatar", function (error, response, body) {
    //     if (!error) {
    //       expect(response.statusCode).to.equal(200);
    //       done();
    //     } else {
    //       console.log("Error : " + error);
    //     }
    //   });
    // });

    // it("POST->/upload_profil_picture", function(done) {
    //   request.post(
    //     base_url + "/upload_profil_picture",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         name: "admin"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         // console.log(body)
    //         done();
    //       } else {
    //         console.log(error);
    //       }
    //     }
    //   );
    // });

    // it("GET->/verify", function(done) {
    //   request.get(
    //     base_url + "/verify?id=0",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         expect(body).contain("Account");
    //         done();
    //       } else {
    //         console.log(error);
    //       }
    //     }
    //   );
    // });

    // it("GET->/users", function(done) {
    //   request.get(
    //     base_url + "/users",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         expect(JSON.parse(response.body)[0].active).is.true;
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("POST->/users", function(done) {
    //   request.post(
    //     base_url + "/users",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         name: "test",
    //         lastname: "test",
    //         surname: "test",
    //         mail: "test",
    //         password: "test"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         expect(response.body.message).contain("Wrong");
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("GET->/users/:mail", function(done) {
    //   request.get(
    //     base_url + "/users/admin@admin.fr",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         name: "test",
    //         lastname: "test",
    //         surname: "test",
    //         mail: "test",
    //         password: "test"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         // console.log(response.body)
    //         expect(response.body.active).is.true;
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("PUT->/users/:mail", function(done) {
    //   request.put(
    //     base_url + "/users/admin@admin.fr",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         surname: "admin"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         expect(response.body.message).contain("edited");
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("POST->/position", function(done) {
    //   request.post(
    //     base_url + "/position",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         mail: "admin@admin.fr",
    //         position: "test_pos"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         // console.log(response.body)
    //         expect(response.body.position).equal("test_pos");
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("POST->/getId", function(done) {
    //   request.post(
    //     base_url + "/getId",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         mail: "admin"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         // console.log(response.body)
    //         expect(response.body.id).not.null;
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("POST->/sendMessage", function(done) {
    //   request.post(
    //     base_url + "/chat/sendMessage",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         msg_type: 1,
    //         msg_text: "test",
    //         user: "test",
    //         target: "test"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         // console.log(response.body)
    //         expect(response.body.msg_text).equal("test");
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });

    // it("POST->/getMessages", function(done) {
    //   request.post(
    //     base_url + "/chat/getMessages",
    //     {
    //       headers: {
    //         Authorization: "Bearer " + token
    //       },
    //       json: {
    //         mail: "test"
    //       }
    //     },
    //     function(error, response, body) {
    //       if (!error) {
    //         expect(response.body.message[0].msg_text).equal("test");
    //         done();
    //       } else {
    //         console.log("Error : " + error);
    //       }
    //     }
    //   );
    // });
  });
});
